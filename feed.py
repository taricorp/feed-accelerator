#!/usr/bin/env python
## Requires Python 2.6 or better and Universal Feed Parser
## (pip install feedparser)
"""Provides RSS/Atom feed reading and formatting."""

# Core modules
import configparser
from configparser import NoOptionError, NoSectionError
import signal
import time
import datetime
import io
import threading
import tempfile
import sys
import feedparser
# Output formatters
import subprocess
import shlex
import os
import codecs
import urllib.request as urllib
import re
import shutil

__author__ = "Peter Marheine"
__copyright__ = "Copyright 2011, Peter Marheine"
__license__ = "BSD"
__version__ = "0.1"
__email__ = "peter@taricorp.net"

## TODO:
# - Implement an output arbiter capable of buffering feed entries and
#   staggering output such that a feed refresh can't flood the output
#   stream with many entries.  Also interleave entries from multiple feeds
#   when they're buffered based on the publish time so they follow
#   proper chronological order.

## Config file:
# The default section contains the following options:
# - last_update - timestamp (as provided by Python's time.time()) of the
#                 last run of the reader.  The reader uses this to filter
#                 feed entries which have already been shown, and updates it
#                 on exit.
#
# All non-default sections of the file each describe a single feed, with the
# following required options:
# - uri - URI of the feed.  Usually http://example.net/rss or some such.
#
# The following options are optional within a feed section:
# - interval - time in minutes to wait between feed refreshes.  Defaults to
#              to 30 minutes.

# Log levels and convenience functions for writing to stderr.
LOG_ERROR = 0
LOG_INFO = 1
LOG_VERBOSE = 2
LOG_LEVEL = LOG_INFO
def _debug(msg, level):
    if LOG_LEVEL >= level:
        print(msg, file=sys.stderr)
def _error(msg):
    _debug(msg, LOG_ERROR)
def _info(msg):
    _debug(msg, LOG_INFO)
def _verbose(msg):
    _debug(msg, LOG_VERBOSE)
def _abort(msg, status=1):
    _error("Giving up: {0}".format(msg))
    sys.exit(status)
                
def main(ini_file='feed.py.ini'):
    config = configparser.ConfigParser()
    config.read(ini_file)
    
    # Output formatter
    try:
        output_mode = config.get('feed-accelerator', 'output_mode')
    except (NoOptionError, NoSectionError):
        _info("Output mode not set, defaulting to 'raw'.")
        output_mode = 'raw'
    if output_mode not in OUTPUT_MODES:
        _error("Unknown output mode, falling back to 'raw'.")
    formatter = OUTPUT_MODES.get(output_mode, OUTPUT_MODES['raw'])
    # Output buffering
    try:
        buffer = config.getboolean('feed-accelerator', 'buffer')
    except (NoOptionError, NoSectionError, ValueError):
        _info("buffer not set or nonsense, defaulting to 'no'.")
        buffer = False
    # Output formatter overrides this with file from output_options
    output = sys.stdout
    
    # Create Accelerator
    output_options = dict(config.items('output'))
    formatter = formatter(**output_options)
    if buffer:
        formatter = TemporalBuffer(formatter)
    accelerator = FeedAccelerator(formatter)
    sections = config.sections()
    try:
        sections.remove('feed-accelerator')
        sections.remove('output')
    except ValueError:
        pass
    if len(sections) is 0:
        _error("No feeds defined; now exiting.")
        sys.exit(1)
    # Add feeds
    for feed in sections:
        params = dict(config.items(feed))   # Includes members of [DEFAULT]
        uri = params.pop('uri')
        _verbose("Adding feed %s (%s)" %(feed, uri))
        accelerator.add_feed(uri, name=feed, **params)
    
    signal.signal(signal.SIGINT, lambda sig,sf: None)
    signal.pause()
    accelerator.kill()
    _info("Saving settings..")
    config.set('DEFAULT', 'last_update', str(time.time()))
    with open(ini_file, 'w') as f:
        config.write(f)

class FeedAccelerator(object):
    """Base feed watcher class."""
    
    def __init__(self, output):
        """Returns new FeedAccelerator writing to specified stream.
        
        output -- OutputFormatter to write entries to."""
        self._watchers = []
        self._lock = threading.Lock()
        self._death_clock = threading.Condition()
        self.output = output
    
    def add_feed(self, uri, **kwargs):
        """Add a new feed to watch.
        
        uri -- URI of the feed.
        
        Keyword arguments:
        interval -- number of minutes to wait between fetches
        last_update -- timestamp of last fetch (as from time.time())
        name -- name to give the worker thread
        """
        w = _FeedWatcher(uri, self._death_clock, out=self.output,
                         lock=self._lock, **kwargs)
        w.start()
        self._watchers.append(w)
    
    def kill(self):
        """Stops all worker threads and blocks until they terminate."""
        for w in self._watchers:
            _verbose("killing %s" %w)
            w.die()
        self._death_clock.acquire()
        self._death_clock.notifyAll()
        self._death_clock.release()
        for w in self._watchers:
            w.join()


class _FeedWatcher(threading.Thread):
    """Worker which watches a single feed and writes new entries to an output
       stream when they are noticed."""
    def __init__(self, uri, death_clock, out, interval=15,
                 last_update=0, name=None, lock=None, prefetch="0"):
        """uri - URI to fetch the feed from.
           death_clock - threading.Condition used to notify the worker that
                         termination is desired.
           interval - time in minutes to wait between fetches.
           out - stream to write feed to.
           last_update - time (as provided by time.mktime) of last update.  0
                         means never.
           name - thread name to assign, as in threading.Thread().
           lock - lock for synchronizing access to out.  If None, this will
                  not perform any locking."""
        super(_FeedWatcher, self).__init__(name=name)
        self.uri = uri
        self.death_clock = death_clock
        self.interval = int(interval)
        self.out = out
        self.lock = lock
        self.last_update = float(last_update)
        if self.last_update > time.time():
            _error("last_update is in the future; ignoring.")
            self.last_update = 0.0
        try:
            self.prefetch = bool(int(prefetch))
        except ValueError:
            _info("Prefetch '{0}' doesn't make sense, defaulting to off.".format(prefetch))
            self.prefetch = False
        self.prefetch = prefetch
        self._die = False
        
    def die(self):
        """Sets the die flag.  Caller should also call death_clock.notify or
           notifyAll to actually kill the watcher."""
        self._die = True
        
    def run(self):
        while not self._die:
            # Update feed
            _verbose("[%s] Updating %s" %(datetime.datetime.now(), self.name))
            if self.prefetch:
                _verbose("Open URI")
                response = urllib.urlopen(self.uri)
                _verbose("Open tempfile")
                temp = tempfile.NamedTemporaryFile()
                _verbose("Copy")
                shutil.copyfileobj(response, temp)
                uri = temp.name
                _verbose("Prefetch on, using {0}".format(temp.name))
            else:
                uri = self.uri
            feed = feedparser.parse(uri)
            if self.prefetch:
                # Tempfile unlinks when garbage collected
                del temp
            # Changed since last fetch?
            stamp = float('Inf')
            try:
                stamp = time.mktime(feed['updated'])
            except KeyError:
                # FIXME Looks like only RSS has this key?
                _verbose("No updated tag; assuming something new")
            _verbose("I last updated at {0}".format(self.last_update))
            if self.last_update < stamp:
                # Output newer entries
                for entry in reversed(feed.entries):
                    update_time = time.mktime(entry.updated_parsed)
                    _verbose("Entry updated at {0}".format(update_time))
                    if time.mktime(entry.updated_parsed) < self.last_update:
                        _verbose("skip")
                        continue
                    self._output(entry)
                # Update timestamp
                self.last_update = stamp
            else:
                _verbose("Nothing new")
            # Wait for next update or termination notification
            self.death_clock.acquire()
            self.death_clock.wait(self.interval * 60)
            self.death_clock.release()
    
    def _output(self, entry):
        """Writes feed entry to the output stream in a thread-safe manner."""
        o = self.out
        if self.lock:
            self.lock.acquire()
        o.write(entry)
        o.flush()
        if self.lock:
            self.lock.release()
           
class TemporalBuffer(object):
    pass

class OutputFormatter(object):
    """Base class for formatting feed entries to output.
       Provides self.out, which is a text-oriented stream writer."""
    file_output = False
    def __init__(self, out, buffer=False, errors='replace'):
        # Wrap output stream if it's byte-oriented
        if isinstance(out, (io.RawIOBase, io.BufferedIOBase)):
            out = codecs.getwriter('utf-8')(out, errors=errors)
        self.out = out
    def flush(self):
        self.out.flush()
       
class HTMLFileOutputFormatter(OutputFormatter):
    """Writes output to a HTML file."""
    file_output = True
    html_header = "<html><head><title>HTMLOutput</title></head>"
    html_footer = "</html>"
    def __init__(self, out, **kwargs):
        """out -- file to write to.  Path to file if a string, otherwise
                 assumed to be a file handle."""
        if isinstance(out, str):
            out = open(out, 'w')
        out.write(self.html_header)
        super(HTMLFileOutputFormatter, self).__init__(out, **kwargs)
    def write(self, entry):
        self.out.seek(len(self.html_header))
        self.out.write('<h1>%s</h1>\n' %entry.title)
        self.out.write(entry.content[0].value)
        self.out.write(self.html_footer)
        self.out.truncate()
        self.out.flush()

class RawOutputFormatter(OutputFormatter):
    """Raw HTML output, one entry per line."""
    file_output = False
    def write(self, entry):
        r = lambda s: s.replace('\n','')    # Strip newlines
        self.out.write("<h1>%s</h1>" %r(entry.title))
        self.out.write(r(entry.content[0]['value']))
        self.out.write("\n")
        
class UZBLOutputFormatter(RawOutputFormatter):
    """Displays single entry at a time in an instance of uzbl-core."""
    file_output = False
    def __init__(self, out, **kwargs):
        PIPE, DEVNULL = subprocess.PIPE, open(os.devnull, 'w')
        uzbl = subprocess.Popen(shlex.split('/usr/bin/uzbl-core -c -'), stdin=PIPE, stdout=DEVNULL)
        _verbose("Launched uzbl-core with PID %i" %uzbl.pid)
        self.uzbl = uzbl
        # Superclass handles real output formatting
        super(UZBLOutputFormatter, self).__init__(uzbl.stdin, errors='xmlcharrefreplace', **kwargs)
        # Make uzbl prettier
        ## TODO: move this out to the ini file?
        self.out.write("set show_status=0\n")
        self.out.write("set geometry=1024x768\n")
    def write(self, entry):
        if self.uzbl.poll() is not None:
            raise IOError("UZBL terminated unexpectedly. (%i)" %self.uzbl.returncode)
        self.out.write("set inject_html=")
        super(UZBLOutputFormatter, self).write(entry)
        self.flush()

class TorrentOutputFormatter(OutputFormatter):
    file_output = False
    def __init__(self, **kwargs):
        self.out_dir = kwargs['directory']
    
    def write(self, entry):
        url = entry.link
        _verbose("Downloading torrent: {0}".format(url))
        f = urllib.urlopen(url) 
        headers = f.info()
        if headers['Content-Type'] != 'application/x-bittorrent':
            print("Skipped URL {0}: bad content type".format(url))
            return
        filename = re.search('filename="([^"]+)"', headers['Content-Disposition'])
        if filename is None:
            print("Content-Disposition mismatch, ignoring {0}".format(url))
            return
        filename = filename.group(1)
        with open(os.path.join(self.out_dir, filename), 'wb') as f_out:
            shutil.copyfileobj(f, f_out)
    
    def flush(self):
        pass


OUTPUT_MODES = {'uzbl': UZBLOutputFormatter,
                'raw': RawOutputFormatter,
                'html': HTMLFileOutputFormatter,
                'torrent': TorrentOutputFormatter,}
        
if __name__ == '__main__':
    if __file__ is not None:
        main(os.path.join(os.path.dirname(__name__),'feed.py.ini'))
    else:
        main()
